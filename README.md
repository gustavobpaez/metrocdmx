# MetroCDMX

``` 
curl --location --request POST 'http://localhost:8080/estaciones' \
--header 'Content-Type: application/json' \
--data-raw '{
   "estacionInicial":"Pantitlán",
   "estacionFinal":"Hangares"
}'
```
Resultado de ejemplo:

``` 
 De la estación 'Pantitlán'de la 'Línea 1' dirigirse a Jamaica
 Transbordar a la 'Línea 4'
 De la estación 'Jamaica'de la 'Línea 4' dirigirse a Tacubaya
 Transbordar a la 'Línea 1'
 De la estación 'Tacubaya'de la 'Línea 1' dirigirse a Mixcoac

 ``` 