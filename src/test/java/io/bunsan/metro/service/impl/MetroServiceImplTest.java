package io.bunsan.metro.service.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.bunsan.metro.algoritm.Graph;
import io.bunsan.metro.exception.GeneralException;
import io.bunsan.metro.model.Line;
import io.bunsan.metro.model.Metro;
import io.bunsan.metro.model.Station;
import io.bunsan.metro.service.LoadFileService;
import io.bunsan.metro.service.MetroService;
import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
class MetroServiceImplTest {

  @Autowired
  private LoadFileService loadFileService;

  @Autowired
  private MetroService service;

  private Metro metro;

  @BeforeEach
  void setUp(final TestInfo testInfo) {
    String pathNameFile = "./src/test/resources/Metro_CDMX.kml";
    this.metro = loadFileService.loadFile(pathNameFile);
    log.info(">>> BeginingTest [{}]", testInfo.getDisplayName());
  }

  @AfterEach
  void tearDown(final TestInfo testInfo) {
    log.info("<<< FinishedTest [{}]", testInfo.getDisplayName());
    this.metro = null;
  }

  @Test
  void testGetStationsByLinea() {
    String nameStation = "Línea A";
    Line lineFindA = service.getStationsByLinea(metro, nameStation);
    log.info("lineFindA:" + lineFindA);
    assertNotNull(lineFindA, "Finded!");
  }

  @Test
  void testGetStationsByLineaNull() {
    String nameStation = "Línea Z";
    GeneralException exception = assertThrows(GeneralException.class, () -> {
      service.getStationsByLinea(metro, nameStation);
    });
    log.debug(exception.getMessage());
  }

  @Test
  void testGetLineasOfStation() {
    List<String> linesOfStation = service.getLineasOfStation(metro, "Pantitlán");
    log.info("linesOfStation: " + linesOfStation);
    assertNotNull(linesOfStation, "Finded!");
  }

  @Test
  void testGetFirstLineaOfStation() {
    String stationName = "Pantitlán";
    log.info(" metro:" + metro);
    Line lineToFind = service.getFirstLineaOfStation(this.metro, stationName);
    log.info(" lineToFind:" + lineToFind);
    assertNotNull(lineToFind, "Finded!");
  }

  @Test
  void testGetStationIntoLinea() {
    Line lineToFind = service.getStationsByLinea(metro, "Línea 9");
    Station stationInitial = service.getStationIntoLinea(lineToFind, "Puebla");
    log.info(" stationInitial:" + stationInitial);
    assertNotNull(stationInitial, "Finded!");
  }

  @Test
  void testGetStationIntoLineaNotFound() {
    Line lineToFind = new Line();
    lineToFind.setName("Línea Z");

    GeneralException exception = assertThrows(GeneralException.class, () -> {
      service.getStationIntoLinea(lineToFind, "Puebla");
    });
    log.debug(exception.getMessage());
  }

  @Test
  void testGetStationIntoLineaButStationNotFound() {
    Line lineToFind = new Line();
    lineToFind.setName("Línea 9");

    GeneralException exception = assertThrows(GeneralException.class, () -> {
      service.getStationIntoLinea(lineToFind, "San Lanzaro");
    });
    log.debug(exception.getMessage());
  }

  @Test
  void testFindAristas() {
    List<Station> aristas = service.findAristas(this.metro);
    log.info(" aristas:" + aristas);
    assertNotNull(aristas, "Finded!");
  }

  @Test
  void testFillMatriz() {
    Line lineFind9 = service.getStationsByLinea(metro, "Línea 9");
    log.info(" lineFind:" + lineFind9);

    Line lineFind5 = service.getStationsByLinea(metro, "Línea 5");
    log.info(" lineFind5:" + lineFind5);

    Station stationInitial = service.getStationIntoLinea(lineFind9, "Puebla");
    log.info(" stationInitial:" + stationInitial);

    Station stationToFind = service.getStationIntoLinea(lineFind5, "Hangares");
    log.info(" stationFind:" + stationToFind);

    List<Station> aristas = new ArrayList<Station>();
    aristas.add(stationInitial);
    aristas.add(stationToFind);
    aristas.addAll(service.findAristas(metro));

    int matriz[][] = new int[aristas.size()][aristas.size()];
    service.fillMatriz(metro, matriz, aristas, stationInitial, stationToFind);
    assertNotNull(matriz, "Finded!");

    service.printStation(aristas);
    service.printMatriz(matriz);

  }

  @Test
  void testPrintStation() {
    List<Station> aristas = service.findAristas(metro);
    assertNotNull(aristas, "Finded!");
    service.printStation(aristas);
  }

  @Test
  void testPrintMatriz() {
    service.printMatriz(MetroServiceImplTest.matrizA);
  }

  @Test
  void testPopulateMatriz() {
    List<Station> aristas = new ArrayList<Station>();
    aristas.addAll(service.findAristas(metro));

    Graph graph = service.populateMatriz(MetroServiceImplTest.matrizA, aristas);

    Graph.showEdges();
  }

  @Test
  void testGetPath() {
    String stationInitialString = "Puebla";
    String stationToFind = "Hangares";

    String indications = service.getPath(stationInitialString, stationToFind);
    log.info(indications);
  }

  private static final int MAX_VALUE = 999999999;

  private static int matrizA[][] = { { 0, 3, 4, MAX_VALUE, 8, MAX_VALUE },
      { MAX_VALUE, 0, MAX_VALUE, MAX_VALUE, 5, MAX_VALUE },
      { MAX_VALUE, MAX_VALUE, 0, MAX_VALUE, 3, MAX_VALUE },
      { MAX_VALUE, MAX_VALUE, MAX_VALUE, 0, MAX_VALUE, MAX_VALUE },
      { MAX_VALUE, MAX_VALUE, MAX_VALUE, 7, 0, 3 },
      { MAX_VALUE, MAX_VALUE, MAX_VALUE, 2, MAX_VALUE, 0 } };

}
