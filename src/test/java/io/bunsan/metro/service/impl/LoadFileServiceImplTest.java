package io.bunsan.metro.service.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.bunsan.metro.model.Metro;
import io.bunsan.metro.service.LoadFileService;
import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
//@ExtendWith(SpringExtension.class)
class LoadFileServiceImplTest {

  @Autowired
  private LoadFileService loadFileService;

  @Test
  void testLoadFile() {
    String pathNameFile = "./src/test/resources/Metro_CDMX.kml";
    log.info("pathName:{}",pathNameFile);
    Metro metro = loadFileService.loadFile(pathNameFile);
    
    log.info(metro.toString());
    assertFalse(metro.getLineas().isEmpty());
    
    
  }

}
