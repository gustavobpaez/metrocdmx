package io.bunsan.metro.model;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class Station implements Cloneable {
  private Integer id;
  private String name;
  private String description;
  private String coordinates;
  private Station stationPrevious;
  private Station stationPreviousDirection;
  private Station stationNext;
  private Station stationNextDirection;
  private Map<String, Integer> lineTrans = new HashMap<String, Integer>();

  public Boolean isPreviousExist() {
    return this.stationPrevious != null;
  }

  public Boolean isNextExist() {
    return this.stationNext != null;
  }

  public Boolean hasLineTrans() {
    return this.lineTrans.size() > 1;
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  @Override
  public String toString() {
    return "Station [id=" + id + ", name=" + name + ", description=" + description
        + ", coordinates=" + coordinates + ", stationPrevious="
        + (stationPrevious != null ? stationPrevious.getName() : "") + ", stationNext="
        + (stationNext != null ? stationNext.getName() : "") + ", stationPreviousDirection="
        + (stationPreviousDirection != null ? stationPreviousDirection.getName() : "")
        + ", stationNextDirection="
        + (stationNextDirection != null ? stationNextDirection.getName() : "") + ", lineTrans="
        + lineTrans + "]";
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Station other = (Station) obj;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }
}
