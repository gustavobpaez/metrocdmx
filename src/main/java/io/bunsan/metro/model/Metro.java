package io.bunsan.metro.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Metro {
  private String name;
  private List<Line> lineas = new ArrayList<Line>();

}
