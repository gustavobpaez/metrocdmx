package io.bunsan.metro.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class Line {
  private Integer id;
  private String name;
  private List<Station> stations = new LinkedList<Station>();
  private List<String> coordinates = new LinkedList<String>();

  // key is line
  // Station relationated to line
  private Map<Integer, List<Station>> stationsTrans = new HashMap<Integer, List<Station>>();

}
