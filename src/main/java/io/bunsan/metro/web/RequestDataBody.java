package io.bunsan.metro.web;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RequestDataBody implements Serializable {
  private static final long serialVersionUID = -4350245662836725507L;

  @NotEmpty
  private String estacionInicial;

  @NotEmpty
  private String estacionFinal;

}
