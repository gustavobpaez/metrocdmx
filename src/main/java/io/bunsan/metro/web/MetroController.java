package io.bunsan.metro.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.bunsan.metro.service.MetroService;

@RestController
public class MetroController {

  @Autowired
  private MetroService service;

  @PostMapping("/estaciones")
  public String getStations(@RequestBody RequestDataBody requestDataBody) throws Exception {
    return service.getPath(requestDataBody.getEstacionInicial(),
        requestDataBody.getEstacionFinal());
  }

}
