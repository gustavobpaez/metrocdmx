package io.bunsan.metro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class MetroApplication {

  public static void main(String[] args) {
    try {
      SpringApplication.run(MetroApplication.class, args);
    } catch (Exception e) {
      log.error("Exception into SpringApplication.run(MetroApplication.class):{}", e.getMessage(), e);
    }
  }
}
