package io.bunsan.metro.service;

import java.util.List;

import io.bunsan.metro.algoritm.Graph;
import io.bunsan.metro.exception.GeneralException;
import io.bunsan.metro.model.Line;
import io.bunsan.metro.model.Metro;
import io.bunsan.metro.model.Station;

/**
 * 
 * @author gbp
 *
 */
public interface MetroService {

  String getPath(String stationInitialString, String stationToFind) throws GeneralException;

  Line getStationsByLinea(Metro metro, String nameLinea);

  List<String> getLineasOfStation(Metro metro, String stationName);

  Line getFirstLineaOfStation(Metro metro, String stationName);

  /**
   * Es necesario validar que exista la estacion.
   * 
   * @param line
   * @param nameStation
   * @return
   */
  Station getStationIntoLinea(Line line, String nameStation);

  List<Station> findAristas(Metro metro);

  void fillMatriz(Metro metro, int[][] matriz, List<Station> aristas, Station stationInitial,
      Station stationFind);

  void printStation(List<Station> stations);

  void printMatriz(int[][] matriz);

  Graph populateMatriz(int matriz[][], List<Station> aristas);

}
