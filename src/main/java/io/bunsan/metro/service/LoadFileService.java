package io.bunsan.metro.service;

import io.bunsan.metro.exception.GeneralException;
import io.bunsan.metro.model.Metro;


public interface LoadFileService {

  /**
   * 
   * @param pathNameFile
   * @return
   */
  Metro loadFile(String pathNameFile);

  /**
   * 
   * @return
   * @throws GeneralException
   */
  Metro loadFile() throws GeneralException;

  Metro getMetro();

}
