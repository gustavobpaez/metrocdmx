package io.bunsan.metro.service.impl;

import java.io.File;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.xml.sax.InputSource;

import com.sun.codemodel.JCodeModel;
import com.sun.tools.xjc.api.S2JJAXBModel;
import com.sun.tools.xjc.api.SchemaCompiler;
import com.sun.tools.xjc.api.XJC;

import io.bunsan.metro.exception.GeneralException;
import io.bunsan.metro.service.ObjectGenerationService;

//@Configuration
public class ObjectGenerationServiceImpl
    implements ObjectGenerationService, BeanFactoryPostProcessor {

  @Value("${xsd.path.filename}")
  private String xsdNameFile;

  @Value("${classes.pathGenerate}")
  private String pathGenerate;

  @Override
  public void generateClasesFromXSD(String xsdNameFileP, String pathGenerateP) throws Exception {
    if (xsdNameFileP == null || xsdNameFileP.isEmpty() || xsdNameFileP.isBlank()
        || pathGenerateP == null || pathGenerateP.isEmpty() || pathGenerateP.isBlank()) {
      throw new GeneralException("Insufficient parameters");
    }
    File xsdFile = new File(this.xsdNameFile);
    SchemaCompiler sc = XJC.createSchemaCompiler();
    sc.parseSchema(new InputSource(xsdFile.toURI().toString()));
    S2JJAXBModel model = sc.bind();
    JCodeModel cm = model.generateCode(null, null);
    cm.build(new File(pathGenerateP));
  }

  @Override
  public void generateClasesFromXSD() throws Exception {
    this.xsdNameFile = "./src/main/resources/MetroSchema.xsd";
    this.pathGenerate = "./src/";
    this.generateClasesFromXSD(this.xsdNameFile, this.pathGenerate);

  }

  @Override
  public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory)
      throws BeansException {
    try {
      generateClasesFromXSD();
    } catch (Exception e) {
      throw new GeneralException(e.getMessage(), e);
    }

  }
}
