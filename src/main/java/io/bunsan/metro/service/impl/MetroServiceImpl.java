package io.bunsan.metro.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.bunsan.metro.algoritm.Graph;
import io.bunsan.metro.exception.GeneralException;
import io.bunsan.metro.model.Line;
import io.bunsan.metro.model.Metro;
import io.bunsan.metro.model.Station;
import io.bunsan.metro.service.LoadFileService;
import io.bunsan.metro.service.MetroService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MetroServiceImpl implements MetroService {

  private static final int MAX_VALUE = 999999999;
  private static final int POTITION_INITIAL = 0;
  private static final int POTITION_TO_FIND = 1;
  private static final String COMA = ",";
  private static final String WORD_ONE =  "\n De la estaci\u00F3n '";
  private static final String WORD_TWO = "de la '";
  private static final String WORD_THREE = " dirigirse a ";
  private static final String WORD_FOUR = "\n Transbordar a la '";
  private static final String WORD_APOST = "'";
  @Autowired
  private LoadFileService loadFileService;
  
  @Override
  public String getPath(String stationInitialString, String stationToFindString) throws GeneralException{

    List<String> indications=null;
    String indicationString=null;
    
    Metro metro = loadFileService.getMetro();
    
    //TODO validation parameters Excepciones
    Line lineInitial = this.getFirstLineaOfStation(metro, stationInitialString);
    Line lineToFind = this.getFirstLineaOfStation(metro, stationToFindString);
    
    Station stationInitial = this.getStationIntoLinea(lineInitial, stationInitialString);
    Station stationToFind = this.getStationIntoLinea(lineToFind, stationToFindString);
    
    //TODO Si las estaciones estan en la misma linea, no hacer todo el algoritmo, solo recorrerlos.
    
    List<Station> aristas = new ArrayList<Station>();
    aristas.add(stationInitial);
    aristas.add(stationToFind);
    aristas.addAll(this.findAristas(metro));
    
    int matriz[][] = new int[aristas.size()][aristas.size()];
    this.fillMatriz(metro, matriz, aristas,stationInitial,stationToFind);
    
    this.printStation(aristas);
    this.printMatriz(matriz);
    
    Graph graph = this.populateMatriz(matriz, aristas);
    indications = Arrays.asList(graph.dijkStra(POTITION_INITIAL));
    if(indications.isEmpty() || indications.size()<=1) {
      throw new GeneralException("Error to find path");
    }
    //The indicattions it is in position POTITION_TO_FIND
    String stations = indications.get(POTITION_TO_FIND);
    List<String> indicationsToFollow =  Arrays.asList(stations.split(COMA));
//    indicationsToFollow.forEach(ind ->{
//      log.info(ind);
//    });
    
    indicationString = indicationByAristas(metro, matriz, lineInitial, aristas, indicationsToFollow);
    
    return indicationString;
  }
  
  private String indicationByAristas(Metro metro, 
      int matriz[][], Line line, List<Station> aristas, List<String> indicationsToFollow) {
    String nameStationDest = null;
    Line lineFollow = null;
    Station stationFollow = null;
    int pos = 0;
    StringBuilder indications = new StringBuilder();
    String nameStation = indicationsToFollow.get(pos);
    lineFollow = line;
    
    for(pos = 0; pos < indicationsToFollow.size()-1; pos++) {
      indications.append(WORD_ONE).append(nameStation).append(WORD_APOST);
      indications.append(WORD_TWO).append(lineFollow.getName()).append(WORD_APOST);
      
      if(pos+1<indicationsToFollow.size()) {
        //Hay siguiente
        nameStationDest = indicationsToFollow.get(pos+1);
        indications.append(WORD_THREE).append(nameStationDest);
        
        //Buscar las lineas que estación origen tienen transbordo deferente a la actual.
        List<String> lineasTrans = this.getLineasOfStation(metro, nameStationDest);        
        for (String nameLineTrans : lineasTrans) {
          if(!lineFollow.getName().equals(nameLineTrans)) {
            lineFollow = this.getStationsByLinea(metro, nameLineTrans);
            stationFollow = this.findStationInLinea(lineFollow, nameStationDest);
            if(stationFollow!=null) {
              break;
            }
          }
        }
        
        if(stationFollow!=null && lineFollow !=null && (pos+2)<indicationsToFollow.size() ) {
          indications.append(WORD_FOUR).append(lineFollow.getName()).append(WORD_APOST);
          nameStation =stationFollow.getName();
        }
      }
    }
    return(indications.toString());
  }
  
  
  @Override
  public Line getStationsByLinea(Metro metro, String nameLinea) {
    Line lineFinded = null;
    try {
      lineFinded = metro.getLineas().stream().filter(line -> line.getName().equals(nameLinea)).findAny().get();
    }catch (NoSuchElementException e) {
      throw new GeneralException("Not Found.", e);
    }
    return lineFinded;
  }

  @Override
  public List<String> getLineasOfStation(Metro metro, String stationName) {
    List<String> lines = new ArrayList<String>();
    for (Line line : metro.getLineas()) {
      List<Station> stations = line.getStations().stream().filter(station -> station.getName().equals(stationName))
          .collect(Collectors.toList());
      if (!stations.isEmpty()) {
        lines.add(line.getName());
      }
    }
    return lines;
  }

  @Override
  public Line getFirstLineaOfStation(Metro metro, String stationName) {
    for (Line line : metro.getLineas()) {
      List<Station> stations = line.getStations().stream().filter(station -> station.getName().equals(stationName))
          .collect(Collectors.toList());
      if (!stations.isEmpty()) {
        return (line);
      }
    }
    return null;
  }
  
  
  @Override
  public Station getStationIntoLinea(Line line, String nameStation) throws GeneralException {
    List<Station> stations =  line.getStations().stream().filter(station -> station.getName().equals(nameStation)).collect(Collectors.toList());
    if (stations.isEmpty()) {
      throw new GeneralException("La estacion no se encuentra en la linea indicada");
    }
    return stations.get(0); 
  }
  @Override
  public List<Station> findAristas(Metro metro) {
    List<Station> aristas = new ArrayList<Station>();
    metro.getLineas().forEach(line->{
//      if(line.getId()==9 || line.getId()==5 ) {
      aristas.addAll((List<Station>) line.getStations().stream()
          .filter(station -> (!station.isNextExist() || !station.isPreviousExist() || station.hasLineTrans()))
          .collect(Collectors.toList()));
//      }
    });
    return aristas.stream().distinct().collect(Collectors.toList());
  }
  
  @Override
  public void fillMatriz(Metro metro, int[][] matriz, List<Station> aristas, Station stationInitial, Station stationFind) {
    int contFile=0, contColumn=0;
    for (Station stationFile : aristas) {
      for (contColumn=contFile; contColumn<aristas.size();contColumn++) {
        matriz[contFile][contColumn] = MAX_VALUE;
          Station stationColumn = aristas.get(contColumn);
          if(stationFile.equals(stationColumn)) {
            matriz[contFile][contColumn] = 0;
          }else {
            //La stationFile is in the same line
            List<Integer> valuesSF = new ArrayList<Integer>(stationFile.getLineTrans().values());
            List<Integer> valuesSC = new ArrayList<Integer>(stationColumn.getLineTrans().values());
            List<Integer> valuesCommon = new ArrayList<Integer>(valuesSF);
            valuesCommon.retainAll(valuesSC);
            for(Integer idLine : valuesCommon) {
              Line line = findLineById(metro, idLine);
              Station station = findStationInLinea(line, stationFile.getName());
              //Contar hacia delante
              int cont=0;
              while(station.isNextExist() && !station.getName().equals(stationColumn.getName())) {
                station =station.getStationNext();
                cont++;
              }
              //Si no se encontro buscar al contario
              if(cont==0) {
                while(station.isPreviousExist() && !station.getName().equals(stationColumn.getName())) {
                  station =station.getStationPrevious();
                  cont++;
                }  
              }
              matriz[contFile][contColumn] = cont;
            }
          }
          matriz[contColumn][contFile] =matriz[contFile][contColumn];
      }
      contFile++;
    }
  }

  @Override
  public void printStation(List<Station> stations) {
    stations.forEach(station->{
      log.info(station.toString());
    });
  }
  
  @Override
  public void printMatriz(int[][] matriz) {
    String values="";
    for (int[] line : matriz) {
      log.info("{");
      
      for (int column : line) {
        values=column+", ";
      }
      log.info(values+"},");
    }
  }
  
  @Override
  public Graph populateMatriz(int matriz[][], List<Station> aristas) {
    Graph graph = new Graph(matriz.length);
    aristas.forEach(station ->{
      graph.addVertax(station.getName());
    });
    
    for (int i = 0; i < matriz.length; i++) {
      for (int j = 0; j < matriz.length; j++) {
        if(matriz[i][j] != 0 && matriz[i][j] != MAX_VALUE ) {
          graph.addEdges(i,j,matriz[i][j]);
        }
      }
    }
    return graph;
  }

  private Line findLineById(Metro metro, Integer idLline) {
    return metro.getLineas().stream().filter(line -> line.getId().equals(idLline)).findAny().get();
  }

  private Station findStationInLinea(Line line, String nameStation) {
    List<Station> stations = line.getStations().stream().filter(station -> station.getName().equals(nameStation))
        .collect(Collectors.toList());
    if(!stations.isEmpty()) {
      return stations.get(0);
    }
    return null;
  }
}
