package io.bunsan.metro.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Configuration;

import io.bunsan.metro.exception.GeneralException;
import io.bunsan.metro.model.Line;
import io.bunsan.metro.model.Metro;
import io.bunsan.metro.model.Station;
import io.bunsan.metro.service.LoadFileService;
import lombok.extern.slf4j.Slf4j;
import net.opengis.kml._2.Kml;
import net.opengis.kml._2.Kml.Document.Folder;

@Configuration
@Slf4j
//@ConfigurationProperties
public class LoadFileServiceImpl implements LoadFileService, BeanFactoryPostProcessor {

  @Value("${kml.path.filename}")
  private String pathNameFile = "./src/main/resources/Metro_CDMX.kml";

  private static final String LOCALPART_NAME = "name";
  private static final String LOCALPART_FOLDER = "Folder";
  private static final String LOCALPART_NAME_LINES = "Líneas de Metro";
  private static final String LOCALPART_NAME_STATIONS = "Estaciones de Metro";

  private Metro metro = null;

  @Override
  public Metro getMetro() {
    return this.metro;
  }

  @Override
  public Metro loadFile() throws GeneralException {
    return this.loadFile(this.pathNameFile);
  }

  @Override
  public Metro loadFile(String pathNameFile) throws GeneralException {
    Kml tagPrincipal = null;

    if (pathNameFile == null || pathNameFile.trim().isEmpty()) {
      throw new GeneralException("Insufficient parameters");
    }
    this.metro = new Metro();

    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Kml.class);
      File xmlFile = new File(pathNameFile);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      tagPrincipal = (Kml) jaxbUnmarshaller.unmarshal(xmlFile);
      List<JAXBElement<?>> lista = tagPrincipal.getDocument().getNameOrDescriptionOrStyle();

      if (lista == null || lista.isEmpty()) {
        throw new GeneralException("Witout information to process");
      }

      this.metro = getNameMetro(lista);
      List<Line> lines = getLines(lista, LOCALPART_NAME_LINES);
      List<Station> stations = getStations(lista, LOCALPART_NAME_STATIONS);

      setStationsIntoLines(this.metro, lines, stations);

      setStationPreviousNextByStation(this.metro);
    } catch (JAXBException e) {
      throw new GeneralException("Exception into loadFile(LoadFileServiceImpl.class)", e);
    }

    return this.metro;
  }

  private Metro getNameMetro(List<JAXBElement<?>> lista) {
    Metro metro = new Metro();
    JAXBElement<?> nameMetro = getElementByName(lista, LOCALPART_NAME, null);
    if (nameMetro != null) {
      metro.setName(nameMetro.getValue().toString());
    }
    return metro;
  }

  // TODO Mejorar for anidados
  private JAXBElement<?> getElementByName(List<JAXBElement<?>> lista, String name,
      String nameFolder) {
    for (JAXBElement<?> jaxbElement : lista) {
      if (jaxbElement.getName().getLocalPart().equals(name)) {
        if (nameFolder == null || nameFolder.isEmpty()) {
          return jaxbElement;
        }
        if (jaxbElement.getValue() instanceof Folder) {
          Folder folder = (Folder) jaxbElement.getValue();
          if (folder.getName().equals(nameFolder)) {
            return jaxbElement;
          }
        }
      }
    }
    return null;
  }

  private List<Line> getLines(List<JAXBElement<?>> lista, String nameFolder) {
    List<Line> lines = new ArrayList<Line>();
    JAXBElement<?> nameFolderElement = getElementByName(lista, LOCALPART_FOLDER, nameFolder);
    Line line = null;

    Folder folder = (Folder) nameFolderElement.getValue();
    List<Kml.Document.Folder.Placemark> lineas = folder.getPlacemark();
    // Comienza en 1
    int cont = 1;
    for (Kml.Document.Folder.Placemark jaxbElement2 : lineas) {
      line = new Line();

      line.setName(jaxbElement2.getName());
      line.setId(cont);

      String cordenates = jaxbElement2.getLineString().getCoordinates();
      List<String> cordenatesList = Stream.of(cordenates.split("\\r?\\n")).map(String::trim)
          .collect(Collectors.toList());
      cordenatesList.removeAll(Collections.singleton(""));
      line.setCoordinates(cordenatesList);
      lines.add(line);
      cont++;
    }
    return lines;
  }

  private List<Station> getStations(List<JAXBElement<?>> lista, String nameFolder) {
    List<Station> stations = new ArrayList<Station>();

    JAXBElement<?> nameMetro = getElementByName(lista, LOCALPART_FOLDER, nameFolder);

    Folder folder = (Folder) nameMetro.getValue();
    List<Kml.Document.Folder.Placemark> estaciones = folder.getPlacemark();
    for (Kml.Document.Folder.Placemark estacion : estaciones) {
      Station station = new Station();
      station.setName(estacion.getName());
      station.setDescription(estacion.getDescription());
      station.setCoordinates(estacion.getPoint().getCoordinates().trim());
      stations.add(station);
    }
    return stations;
  }

  private Metro setStationsIntoLines(Metro metro, List<Line> lines, List<Station> stations) {
    Integer cont = 0;
    List<Line> linesRefactor = new ArrayList<Line>();
    for (Line line : lines) {
      cont = 0;
      Line lineNew = new Line();
      lineNew.setName(line.getName());
      lineNew.setId(line.getId());

      List<String> coordinates = line.getCoordinates();
      for (String coordinate : coordinates) {
        List<Station> stationsFound = stations.stream()
            .filter(station -> station.getCoordinates().equals(coordinate))
            .collect(Collectors.toList());
        Station stat = null;
        if (stationsFound.isEmpty()) {
          cont--;
        } else {

          if (stationsFound.size() == 1) {
            stat = stationsFound.get(0);
            stat.getLineTrans().put(line.getName(), line.getId());

            if (stat.getId() != null) {
              /** TOCLONE **/
              try {
                stat = (Station) stat.clone();
                stat.setId(cont);
                stations.add(stat);
              } catch (CloneNotSupportedException e) {
                e.printStackTrace();
              }
            } else {
              stat.setId(cont);
            }

          } else {
            stationsFound.stream().forEach(station -> {
              station.getLineTrans().put(line.getName(), line.getId());
            });
            /** TOCLONE **/
            try {
              stat = (Station) stationsFound.get(0).clone();
              stat.setId(cont);
            } catch (CloneNotSupportedException e) {
              e.printStackTrace();
            }
            /**/
          }
          lineNew.getStations().add(stat);
          cont++;
        }
      }
      linesRefactor.add(lineNew);
    }
    metro.setLineas(linesRefactor);
    return metro;
  }

  private Metro setStationPreviousNextByStation(Metro metro) {
    int cont = 0;
    Station stationPreviosDirection = null;
    Station stationNextDirection = null;

    for (Line line : metro.getLineas()) {
      stationPreviosDirection = line.getStations().get(0);
      stationNextDirection = line.getStations().get(line.getStations().size() - 1);

      cont = 0;
      for (Station station : line.getStations()) {
        if (cont == 0) {
          station.setStationPrevious(null);
        } else {
          station.setStationPrevious(line.getStations().get(cont - 1));
          station.setStationPreviousDirection(stationPreviosDirection);
        }

        if (cont == line.getStations().size() - 1) {
          station.setStationNext(null);
        } else {
          station.setStationNext(line.getStations().get(cont + 1));
          station.setStationNextDirection(stationNextDirection);
        }
        cont++;
      }
    }

    return metro;
  }

  @Override
  public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory)
      throws BeansException {
    try {
      loadFile(this.pathNameFile);
    } catch (Exception e) {
      log.error("Exception into postProcessBeanFactory(LoadFileServiceImpl.class):{}",
          e.getMessage(), e);
    }

  }

}
