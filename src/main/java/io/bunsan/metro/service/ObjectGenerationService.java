package io.bunsan.metro.service;

/**
 * Inteface to generate Object from to File Schema
 * 
 * @author gbp
 *
 */
public interface ObjectGenerationService {

  /**
   * To generate clases.
   * 
   * @param xsdNameFile  path and name file
   * @param pathGenerate path into src where its going generated.
   * @throws Exception
   */
  void generateClasesFromXSD(final String xsdNameFile, final String pathGenerate) throws Exception;

  /**
   * 
   * @throws Exception
   */
  void generateClasesFromXSD() throws Exception;
}
