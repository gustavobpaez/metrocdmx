package io.bunsan.metro.exception;

import org.springframework.core.NestedRuntimeException;

public class GeneralException extends NestedRuntimeException {

  private static final long serialVersionUID = -9221707338251762625L;

  public GeneralException(String msg, Throwable cause) {
    super(msg, cause);
  }

  public GeneralException(String msg) {
    super(msg);

  }
}
