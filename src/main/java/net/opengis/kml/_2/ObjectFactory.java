//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantacion de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderon si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.10.15 a las 12:40:53 PM CDT 
//


package net.opengis.kml._2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.opengis.kml._2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _KmlDocumentName_QNAME = new QName("http://www.opengis.net/kml/2.2", "name");
    private final static QName _KmlDocumentDescription_QNAME = new QName("http://www.opengis.net/kml/2.2", "description");
    private final static QName _KmlDocumentStyle_QNAME = new QName("http://www.opengis.net/kml/2.2", "Style");
    private final static QName _KmlDocumentStyleMap_QNAME = new QName("http://www.opengis.net/kml/2.2", "StyleMap");
    private final static QName _KmlDocumentFolder_QNAME = new QName("http://www.opengis.net/kml/2.2", "Folder");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.opengis.kml._2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Kml }
     * 
     */
    public Kml createKml() {
        return new Kml();
    }

    /**
     * Create an instance of {@link Kml.Document }
     * 
     */
    public Kml.Document createKmlDocument() {
        return new Kml.Document();
    }

    /**
     * Create an instance of {@link Kml.Document.Folder }
     * 
     */
    public Kml.Document.Folder createKmlDocumentFolder() {
        return new Kml.Document.Folder();
    }

    /**
     * Create an instance of {@link Kml.Document.Folder.Placemark }
     * 
     */
    public Kml.Document.Folder.Placemark createKmlDocumentFolderPlacemark() {
        return new Kml.Document.Folder.Placemark();
    }

    /**
     * Create an instance of {@link Kml.Document.StyleMap }
     * 
     */
    public Kml.Document.StyleMap createKmlDocumentStyleMap() {
        return new Kml.Document.StyleMap();
    }

    /**
     * Create an instance of {@link Kml.Document.Style }
     * 
     */
    public Kml.Document.Style createKmlDocumentStyle() {
        return new Kml.Document.Style();
    }

    /**
     * Create an instance of {@link Kml.Document.Style.IconStyle }
     * 
     */
    public Kml.Document.Style.IconStyle createKmlDocumentStyleIconStyle() {
        return new Kml.Document.Style.IconStyle();
    }

    /**
     * Create an instance of {@link Kml.Document.Folder.Placemark.LineString }
     * 
     */
    public Kml.Document.Folder.Placemark.LineString createKmlDocumentFolderPlacemarkLineString() {
        return new Kml.Document.Folder.Placemark.LineString();
    }

    /**
     * Create an instance of {@link Kml.Document.Folder.Placemark.Point }
     * 
     */
    public Kml.Document.Folder.Placemark.Point createKmlDocumentFolderPlacemarkPoint() {
        return new Kml.Document.Folder.Placemark.Point();
    }

    /**
     * Create an instance of {@link Kml.Document.StyleMap.Pair }
     * 
     */
    public Kml.Document.StyleMap.Pair createKmlDocumentStyleMapPair() {
        return new Kml.Document.StyleMap.Pair();
    }

    /**
     * Create an instance of {@link Kml.Document.StyleMap.LineStyle }
     * 
     */
    public Kml.Document.StyleMap.LineStyle createKmlDocumentStyleMapLineStyle() {
        return new Kml.Document.StyleMap.LineStyle();
    }

    /**
     * Create an instance of {@link Kml.Document.StyleMap.BalloonStyle }
     * 
     */
    public Kml.Document.StyleMap.BalloonStyle createKmlDocumentStyleMapBalloonStyle() {
        return new Kml.Document.StyleMap.BalloonStyle();
    }

    /**
     * Create an instance of {@link Kml.Document.Style.LabelStyle }
     * 
     */
    public Kml.Document.Style.LabelStyle createKmlDocumentStyleLabelStyle() {
        return new Kml.Document.Style.LabelStyle();
    }

    /**
     * Create an instance of {@link Kml.Document.Style.IconStyle.Icon }
     * 
     */
    public Kml.Document.Style.IconStyle.Icon createKmlDocumentStyleIconStyleIcon() {
        return new Kml.Document.Style.IconStyle.Icon();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.opengis.net/kml/2.2", name = "name", scope = Kml.Document.class)
    public JAXBElement<String> createKmlDocumentName(String value) {
        return new JAXBElement<String>(_KmlDocumentName_QNAME, String.class, Kml.Document.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.opengis.net/kml/2.2", name = "description", scope = Kml.Document.class)
    public JAXBElement<String> createKmlDocumentDescription(String value) {
        return new JAXBElement<String>(_KmlDocumentDescription_QNAME, String.class, Kml.Document.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Kml.Document.Style }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.opengis.net/kml/2.2", name = "Style", scope = Kml.Document.class)
    public JAXBElement<Kml.Document.Style> createKmlDocumentStyle(Kml.Document.Style value) {
        return new JAXBElement<Kml.Document.Style>(_KmlDocumentStyle_QNAME, Kml.Document.Style.class, Kml.Document.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Kml.Document.StyleMap }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.opengis.net/kml/2.2", name = "StyleMap", scope = Kml.Document.class)
    public JAXBElement<Kml.Document.StyleMap> createKmlDocumentStyleMap(Kml.Document.StyleMap value) {
        return new JAXBElement<Kml.Document.StyleMap>(_KmlDocumentStyleMap_QNAME, Kml.Document.StyleMap.class, Kml.Document.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Kml.Document.Folder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.opengis.net/kml/2.2", name = "Folder", scope = Kml.Document.class)
    public JAXBElement<Kml.Document.Folder> createKmlDocumentFolder(Kml.Document.Folder value) {
        return new JAXBElement<Kml.Document.Folder>(_KmlDocumentFolder_QNAME, Kml.Document.Folder.class, Kml.Document.class, value);
    }

}
